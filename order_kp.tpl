<div id="form-<?php echo $pre_id; ?>" class="fixed">
  <div class="close-form"></div>
  <div class="cell">
    <div class="form">
    <div class="close"><svg viewBox="0 0 16 16" width="10" height="10"><path d="M9.3 8l6.7 6.7-1.3 1.3L8 9.3 1.3 16 0 14.7 6.7 8 0 1.3 1.3 0 8 6.7 14.7 0 16 1.3 9.3 8z"></path></svg></div>
      <div class="title">Запросить коммерческое предложение</div>
      <div class="scroll">
      <form id="ajax-contact-form" enctype="multiple/form-data"  method="post">
          <div class="form-group">
            <label for="nameFF">Имя:</label>
            <input id="nameFF" name="nameFF" type="text" required class="form-control style="padding: 9px 10px;">
          </div>
          <div class="form-group">
            <label for="contactFF">E-mail:</label>
            <input id="contactFF" name="contactFF" type="email" required class="form-control style="padding: 9px 10px;">
          </div>
          <div class="form-group">
            <label for="telFF">Телефон:</label>
            <input id="telFF" name="telFF" type="tel" required class="form-control style="padding: 9px 10px;">
          </div>
          <div class="form-group">
            <label for="projectFF">Сообщение</label>
            <textarea id="projectFF" name="projectFF" cols="40" rows="3" style="background-color: #f6f6f6;"></textarea>
          </div>
          <div class="control-file" style="padding-bottom: 7px;">
            <label for="fileFF">Прикрепить файл:</label>
            <input id="fileFF" onchange="handleChange" name="fileFF[]" multiple type="file" style="padding: 9px 10px;">
            <button onclick="foo(this)">Очистить</button>
          </div>          
          <div class="text-agree">
            <?php echo $text_agree; ?>
          </div>
          <div class="buttons">
            <button class="btn orang" type="submit" id="submitFF">Отправить сообщение</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  function foo(elem) {
    var prevElem = elem.previousElementSibling;
    var newElem = document.createElement('input');
    newElem.type = 'file';
    elem.parentNode.replaceChild(newElem, prevElem);
  }
</script>

<script>
  $(function() {
    document.getElementById('ajax-contact-form').addEventListener('submit', function(evt){
      var http = new XMLHttpRequest(), f = this;
      var th = $(this);
      evt.preventDefault();
      http.open("POST", "handler.php", true);    
      http.onreadystatechange = function() {
        if (http.readyState == 4 && http.status == 200) {
          $('body').find('#form-<?php echo $pre_id; ?>').remove();
          $.get('index.php?route=popup/popup&type=thanks', function(result) {
          $('body').append(result);
          
        });
        }
      }
      http.onerror = function() {
        alert('Ошибка, попробуйте еще раз');
      }
      http.send(new FormData(f));
    }, false); 
  });
  $('body').on('click', '#form-<?php echo $pre_id; ?> .close-form, #form-<?php echo $pre_id; ?> .close', function(){
  $('body').find('#form-<?php echo $pre_id; ?>').remove();
  $('body').css({overflow: ''});
});
</script>
